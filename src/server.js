const express = require('express');
const cors = require ('cors');

const app = express();
app.use(express.json());
const port = 3000; // You can use any available port
const createError  = require('http-errors');
const route = require('./routes/upload.route');
const route2 = require('./routes/employee.route');
const router3 = require('./routes/product.route');

app.use(cors());
app.use('/upload',route);
app.use('/employee',route2);
app.use('/products',router3);

app.use(function(req, res, next) {
  next(createError(404));
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
