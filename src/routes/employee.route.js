const express = require('express');
const { getEmployeeData } = require('../service/employee.service');
const router2 = express.Router();
const handleResponse = (res, data) => res.status(200).json(data);

router2.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
})

router2.get('/', function(req, res, next) {
    getEmployeeData()
    .then(data=>handleResponse(res,data))
    .catch(next);
});  

module.exports = router2;