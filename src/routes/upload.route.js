const express = require('express');
const { upload } = require('../service/upload.service');
const router = express.Router();

router.use((req, res, next) => {
  console.log('Time: ', Date.now())
  next()
})

router.post('/', upload.single('image'),function(req, res, next) {
    if (!req.file) {
        return res.status(400).json({ error: 'No file uploaded' });
      }
      res.json({ message: 'File uploaded successfully', filename: req.file.filename });
});

module.exports = router;