const express = require('express');
const { getProducts, addProduct } = require('../service/product.service');
const router3 = express.Router();
const handleResponse = (res, data) => res.status(200).json(data);

router3.use((req, res, next) => {
  console.log('Time: ', Date.now())
  next()
})

router3.get('/',function(req, res, next) {
    getProducts()
    .then(data=>handleResponse(res,data))
    .catch(next);
});

router3.post('/add', async (req, res, next) =>{
  addProduct(req.body)
  .then(data=>handleResponse(res,data))
  .catch(next);
});

module.exports = router3;