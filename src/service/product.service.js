const jsonfile = require('jsonfile')
const getProducts=async()=>{
    const file = 'src/assets/data/products.json';
    return jsonfile.readFileSync(file)
}

const addProduct = async(product)=>{
    product.id=Math.floor(Math.random() * 90 + 10);
    return product; 
}


module.exports={getProducts,addProduct};
