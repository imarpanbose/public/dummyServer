const jsonfile = require('jsonfile')

const getEmployeeData= async() =>{
    const file = 'src/assets/data/employees.json';
    return jsonfile.readFileSync(file)
}
module.exports={getEmployeeData};
